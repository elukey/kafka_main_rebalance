#!/bin/bash

set -e

KAFKA_NODE="kafka-main1001.eqiad.wmnet"
# Example THROTTLE="--throttle 10000000"
THROTTLE="--throttle 60000000"
SOURCE_KAFKA_COMMAND="source /etc/profile.d/kafka.sh"

if [ -z "$1" ]; then
    echo "usage: ${0} <topic-name>"
fi

reassign_filename="${1}.json"
reassign_local_path="./topicmappr_json/${reassign_filename}"

if [ ! -f "${reassign_local_path}" ]; then
    echo "Can't find the .json file related to the topic, aborting."
    exit 1
fi

if [ ! -d "./rollback" ]; then
    echo "Can't find the rollback dir, aborting."
    exit 1
fi

if [ ! -d "./completed" ]; then
    echo "Can't find the completed dir, aborting."
    exit 1
fi

echo "Copying json file to kafka.."
scp ${reassign_local_path} $KAFKA_NODE:

echo "Running kafka-reassign-partitions.."
ssh $KAFKA_NODE "${SOURCE_KAFKA_COMMAND}; kafka reassign-partitions --execute --reassignment-json-file ${reassign_filename} ${THROTTLE}" > ./rollback/${reassign_filename}.rollback

echo "Moving file to done.."
mv ${reassign_local_path} ./completed/

echo "Sleep 5 seconds.."
sleep 5

echo "Running first verify command.."
verify_command="ssh ${KAFKA_NODE} \"${SOURCE_KAFKA_COMMAND}; kafka reassign-partitions --reassignment-json-file ${reassign_filename} --verify\""
eval ${verify_command}

echo "You can check the status of the move via: "
echo ${verify_command}
